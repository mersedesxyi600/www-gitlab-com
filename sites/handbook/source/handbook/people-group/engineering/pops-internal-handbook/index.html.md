---
layout: handbook-page-toc
title: "People Group Engineering"
description: "Information about the limited access internal handbook People Operations has."
---

## On this page

{:.no_toc}

- TOC
{:toc}

## Overview
Some of our processes can not be made public and we can't even share them internally. 
For those reasons, People Operations has a internal handbook, where only a limited amount
of people have access.

The project is hosted outside of the `gitlab-com` group (for access reasons) and is available 
[here](https://gitlab.com/gl-people-operations/internal-handbook). However, you need to be 
invited to have access.

Once you have access to the project, you will also have access to the People Operations internal
[handbook](https://gl-people-operations.gitlab.io/internal-handbook/).

## Usage
Only to be used for items that we can't share in the internal only or public handbook. This 
should contain very limit amount of information.

## Get access
Create an access request and assign it to the Director, People Operations. They will add you
as a member to the project. Once you have access to the project, you will also have access 
to the hosted page.
