---
layout: markdown_page
title: "Sales Play: GitOps for First Order"
description: "This page has all the information for the FO GitOps sales play."

---

## On this page
{:.no_toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

- TOC
{:toc .hidden-md .hidden-lg}

### **GitOps Sales Play for FO AEs**

**[View this page in a Google Doc](https://docs.google.com/document/d/1n_8pTn3Hms9cHRJjMTikYA-Mn0y3Fot_2uqm16rWpAc/edit?usp=sharing)**


<table>
  <tr>
   <td>
<h4><strong>Part 1: </strong>Before the Discovery Call</h4>


   </td>
  </tr>
  <tr>
   <td>
<ol>

1.  Familiarize yourself with the <a href="https://about.gitlab.com/handbook/marketing/sales-plays/gitops/">GitOps Sales Play</a> and watch the <a href="https://youtu.be/aXg3zGg54sg">enablement video</a>.
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

2.  Talk to the right persona - typically not a developer, could be a <strong>team lead</strong>, <strong>system architect</strong>, or <strong>cloud architect</strong> from the infrastructure/operations side of the house.
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

3.  Check that the <a href="https://about.gitlab.com/handbook/marketing/sales-plays/gitops/#lim-anchor">last interesting moment</a> and <a href="https://about.gitlab.com/handbook/marketing/sales-plays/gitops/#lim-anchor">top content viewed</a> is related to GitOps.
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

4.  Create a custom pitch deck incorporating these <a href="https://docs.google.com/presentation/d/1YH6zcNQ9EbtvsLsiI3kKb4tMIW3SUwKcUc-7GEK3woQ/edit?usp=sharing">slides</a> as needed. (<a href="https://youtu.be/JtZfnrwOOAw">GitOps messaging</a> and <a href="https://docs.google.com/presentation/d/18cuZjvkMT8uv241dqJZMdaWOyvZiwBOzFvRZ4HaP1iE/edit#slide=id.g8d846209b0_25_172">original deck</a>).
</li>
</ol>
   </td>
  </tr>
</table>



<table>
  <tr>
   <td>
<h4><strong>Part 2: </strong>The Discovery Call</h4>


   </td>
  </tr>
  <tr>
   <td>
<ol>

1.  Listen for these <strong>keywords</strong> to identify if a lead is interested in a GitOps conversation.  
<ul>
 
<li>Infrastructure as code
 
<li>GitOps
 
<li>Infrastructure automation
 
<li>Configuration as code
 
<li>Policy as code
 
<li>Approvals for infrastructure changes
 
<li>Terraform
 
<li>Terraform Cloud
 
<li>Ansible
 
<li>AWS Cloud Formation
 
<li>Weaveworks Flux
 
<li>Argo CD
 
<li>Chef
 
<li>Puppet
 
<li>Salt-Stack
</li> 
</ul>
</li> 
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

2.  Familiarize yourself with the top <a href="https://about.gitlab.com/handbook/marketing/sales-plays/gitops/#value-discovery">discovery questions</a>, <a href="https://about.gitlab.com/handbook/marketing/sales-plays/gitops/#handling-objections">objection handling</a> and <a href="https://about.gitlab.com/handbook/marketing/sales-plays/gitops/#differentiators-how-gitlab-does-it-better">differentiators</a> for GitOps. <strong><em>NOTE: This is very different from other conversations - the persona we talk to is different.</em></strong>
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

3.  Identify <strong>business objectives</strong> and priorities during discovery. These could be: 
<ul>
 
<li>Infrastructure automation
 
<li>Managing cloud native environments
 
<li>Multi cloud / kubernetes adoption
 
<li>Compliance related to infrastructure
</li> 
</ul>
</li> 
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

4.  After your call, follow up with an <a href="https://docs.google.com/document/d/1PSwEBCxkQMgMmtLKocNfP0EKTLnWIr2FZimG0yv1gTM/edit#bookmark=id.vk2ta1k8w0re">email</a>, which should include: 
<ul>
 
<li>Any information they requested (<a href="https://learn.gitlab.com/l/gitops-gtm-content">resources</a> you might include)
 
<li>How that information can be helpful for their research
 
<li>An offer to continue researching these topics on their behalf
 
<li>Mutually agreed upon next steps.
</li> 
</ul>
</li> 
</ol>
   </td>
  </tr>
</table>



<table>
  <tr>
   <td>
<h4><strong>Part 3: </strong>POV & Evaluation</h4>


   </td>
  </tr>
  <tr>
   <td>
<ol>

1.  Set up the evaluation with the appropriate internal resources.
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

2.  Before sending out a trial key: 
<ul>
 
<li>Mutually agree upon “<strong>success criteria</strong>” for the 30-day evaluation period
 
<li>“If at the end of 30-days, we prove [these criteria], then we will consider this trial a success.” Examples of success criteria include:  
<ol>
  
<li>Store and Run an Ansible Playbook in GitLab
  
<li>Incorporate the above into a pipeline
  
<li>Scan x% of our code base (because of the language) 
  
<li>Build a CI pipeline and have a basic one working
</li>  
</ol>
 
3.  Send an <a href="https://docs.google.com/document/d/1PSwEBCxkQMgMmtLKocNfP0EKTLnWIr2FZimG0yv1gTM/edit#bookmark=id.5grp5yaens0e">email</a> outlining and confirming the success criteria.
</li> 
</ul>
</li> 
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

4.  Work with your SA to showcase a technical demo of GitOps and/or share <a href="https://learn.gitlab.com/l/gitops-gtm-content">technical demos, webinars, and other resources</a>.
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

5.  Share <a href="https://about.gitlab.com/handbook/marketing/sales-plays/gitops/#customer-stories">customer stories</a> and <a href="https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/gitops/#gartner-peer-insights">Gartner Peer Insights</a>
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

6.  After each call during the trial period, send an <a href="https://docs.google.com/document/d/1PSwEBCxkQMgMmtLKocNfP0EKTLnWIr2FZimG0yv1gTM/edit#bookmark=id.6tok4lob9uox">email</a> with the progress achieved against the success criteria and what should be worked on the following week. 
</li>
</ol>
   </td>
  </tr>
</table>



<table>
  <tr>
   <td>
<h4><strong>Part 4: </strong>Negotiation / Decision-Making</h4>


   </td>
  </tr>
  <tr>
   <td>
<ol>

1.  Use the “<a href="https://about.gitlab.com/handbook/marketing/sales-plays/gitops/#differentiators-how-gitlab-does-it-better">How GitLab Does it Better</a>” section as supporting material to build the business case for GitLab (ROI) with the customer in the custom deck for the champion present to the economic buyer.
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

2.  Share the <a href="https://learn.gitlab.com/gitops-gtm-all/forrester-tei-blogpost?lx=AZeFEd&search=TEI">Forrester TEI report</a>.
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>
<ol>

3.  Use the <a href="https://about.gitlab.com/calculator/">Cost Comparison Calculators</a> to show the value of using GitLab as a single application over other solutions.
</li>
</ol>
   </td>
  </tr>
  <tr>
   <td>
<h4><strong>Part 5: </strong>Close the Deal!</h4>


   </td>
  </tr>
</table>


